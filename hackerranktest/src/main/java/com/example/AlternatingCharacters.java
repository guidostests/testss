package com.example;

import java.util.Scanner;

/**
 * Created by guido on 12/12/16.
 */

public class AlternatingCharacters {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named GemStones. */

        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        //String input = System.console().readLine();


        int lineNo = Integer.valueOf(input);

        for (int ii = 0; ii < lineNo; ii++) {
            String word = scanner.nextLine();

            System.out.println(countDeletion(word));
        }
    }


    public static int countDeletion(String word) {

        // null string, exit with -1
        if (word == null) {
            return -1;
        }

        int size = word.length();
        // if word is empty or one char, no deletion required
        if (size < 2) {
            return 0;
        }

        char[] chars = word.toCharArray();

        int deletion = 0;
        char lastChar = chars[0]; // this always exists because of the size check above

        // start checking the 2nd char
        for (int ii = 1; ii < size; ii++) {
            if (chars[ii] == lastChar) {
                deletion++;
            } else {
                lastChar = chars[ii];
            }
        }

        return deletion;
    }

}
