package com.example;

import java.util.Scanner;

public class GemStones {
    private static final int ELEMENTS_NO = 26;
    private static final char REF_CHAR = 'a';

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        int[] gemsDictionary = new int[ELEMENTS_NO];

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int lineNo = Integer.valueOf(input);

        for (int ii = 0; ii < lineNo; ii++) {
            String word = scanner.nextLine();

            gemStonesParse(word, gemsDictionary);
        }


        System.out.println(countGemStones(gemsDictionary, lineNo));

    }

    private static void gemStonesParse(String rock, int[] dictionary) {
        int size = rock.length();
        char[] chars = rock.toCharArray();
        int[] internalDictionary = new int[ELEMENTS_NO];

        for (int ii = 0; ii < size; ii++) {
            //char ch = rock.ch[ii];
            internalDictionary[chars[ii] - REF_CHAR]++;
        }

        for (int ii = 0; ii < ELEMENTS_NO; ii++) {
            if (internalDictionary[ii] != 0) {
                dictionary[ii]++;
            }
        }

    }

    private static int countGemStones(int[] dictionary, int noRocks) {

        int gemStonesNo = 0;

        for (int ii = 0; ii < ELEMENTS_NO; ii++) {
            if (dictionary[ii] == noRocks) {
                gemStonesNo++;
            }
        }

        return gemStonesNo;
    }

}


