package com.example;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by guido on 12/12/16.
 */

public class Solution {
/*
 * Complete the function below.
 */

    static int getMinPrice(int requiredSeats, int[][] seatPrices) {

        int minPrice = -1;

        for (int jj = 0; jj < seatPrices.length; jj++) {

            // iterate from the first seat to the last that allows to accomodate all friends
            for (int ii = 0; ii < seatPrices[0].length - requiredSeats + 1; ii++) {

                int tmp = attemptSeatsAllocation(seatPrices, jj, ii, requiredSeats);

                // we only proceed if the seat configuration is good
                if (tmp != -1) {
                    if (minPrice == -1) {
                        // if minPrice isn't assigned
                        minPrice = tmp;
                    } else if (tmp < minPrice) {
                        //if minPriced is assigned and tmp is smaller
                        minPrice = tmp;
                    }
                }
            }
        }
        return minPrice;
    }

    private static int attemptSeatsAllocation(int[][] aSeatPricesMatrix, int y, int x, int friendsNo) {
        // no boundary chek in here, the outher loops need to ensure that

        int totPrice = 0;
        for (int ii = x; ii < x + friendsNo; ii++) {
            //  if a seat is not available then exit
            if (aSeatPricesMatrix[y][ii] == -1) {
                return -1;
            }
            totPrice += aSeatPricesMatrix[y][ii];
        }
        return totPrice;
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        final String fileName = System.getenv("OUTPUT_PATH");
//        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;
        int _requiredSeats;
        _requiredSeats = Integer.parseInt(in.nextLine().trim());


        int _seatPrices_rows = 0;
        int _seatPrices_cols = 0;
        _seatPrices_rows = Integer.parseInt(in.nextLine().trim());
        _seatPrices_cols = Integer.parseInt(in.nextLine().trim());

        int[][] _seatPrices = new int[_seatPrices_rows][_seatPrices_cols];
        for (int _seatPrices_i = 0; _seatPrices_i < _seatPrices_rows; _seatPrices_i++) {
            for (int _seatPrices_j = 0; _seatPrices_j < _seatPrices_cols; _seatPrices_j++) {
                _seatPrices[_seatPrices_i][_seatPrices_j] = in.nextInt();

            }
        }

        if (in.hasNextLine()) {
            in.nextLine();
        }

        res = getMinPrice(_requiredSeats, _seatPrices);

        System.out.println(res);
//        bw.write(String.valueOf(res));
//        bw.newLine();
//
//        bw.close();
    }

    /*
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        // read input ******************************

        String input = scanner.nextLine();
        int friendNo = Integer.valueOf(input);

        input = scanner.nextLine();
        int rowNo = Integer.valueOf(input);

        input = scanner.nextLine();
        int colNo = Integer.valueOf(input);

        int[][] seatPrices = new int[colNo][rowNo];

        for (int ii = 0; ii < rowNo; ii++) {
            String row = scanner.nextLine();

            String[] seats = row.split(" ");

            for (int jj = 0; jj < colNo; jj++) {
                seatPrices[jj][ii] = Integer.valueOf(seats[jj]);
            }
        }

        if (seatPrices.length!=colNo) {
            return;
        }

        if (seatPrices[0].length!=rowNo) {
            return;
        }

        // analysis ***********************************

        int minPrice = -1;

        for (int jj = 0; jj < rowNo; jj++) {

            // iterate from the first seat to the last that allows to accomodate all friends
            for (int ii = 0; ii < colNo - friendNo + 1; ii++) {

                int tmp = attemptSeatsAllocation(seatPrices, ii, jj, friendNo);

                // we only proceed if the seat configuration is good
                if (tmp != -1) {
                    if (minPrice == -1) {
                        // if minPrice isn't assigned
                        minPrice = tmp;
                    } else if (tmp < minPrice) {
                        //if minPriced is assigned and tmp is smaller
                        minPrice = tmp;
                    }
                }
            }
        }
        System.out.println(minPrice);
    }

    private static int attemptSeatsAllocation(int[][] aSeatPricesMatrix, int x, int y, int friendsNo) {
        // no boundary chek in here, the outher loops need to ensure that

        int totPrice = 0;
        for (int ii = x; ii < x+friendsNo; ii++) {
            //  if a seat is not available then exit
            if (aSeatPricesMatrix[ii][y] == -1) {
                return -1;
            }
            totPrice += aSeatPricesMatrix[ii][y];
        }
        return totPrice;
    }
*/
}
